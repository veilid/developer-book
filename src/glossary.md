# Glossary Of Terms

Attachment: The measure of how many connections a node currently has to the Veilid network.

API: Application programming interface. Basically a shared language which allows two or more programs to talk to each other. An API is an agreed-upon format for how requests and responses between them will be shaped, and what information they must contain.

Bootstrap: To initialize a program without prior state data.

Callback function: A function which the application developer writes and which modifies the application state, which is provided to the Veilid library for it to call when network events happen.

Cargo: see “package manager”.

Crate: A single, compilable unit of code in Rust.

Creator keypair: The keypair owned by the creator of a DHT record.

Cryptography: The process of obscuring information (“encrypting”), so that it can only be understood by the intended recipient.

Default schema: A DHT schema type which allows only the creator keypair to write to its value or any of its subkey values.

DNS: Domain name system. A naming scheme for things on the internet, which among other things matches hostnames that humans can easily remember (like google.com) to their corresponding IP addresses (like `172.217.16.238`).

DHT: Distributed Hash Table, a key-value store which stores its keys and values across many nodes, and which provides a means to look them up. In Veilid, every DHT record has a key, a schema, a creator keypair, and subkeys with values. If it uses the Simple schema, it may also have writer keypairs for one or more of its subkeys.

Dialinfo: The collection of a node's public key, location on the physical network, and physical protocols it can accept connections on.

Encryption: see “Cryptography”.

Headless: A headless program runs without a graphical user interface, for example via command line. In the context of Veilid, a node can also be headless; it runs without an application, and its only function is to move traffic.

IP Address: A string of numbers assigned to each device that connects to the internet, which allows it to be uniquely identified.

Internet: a series of tubes.

Keypair: A set of two cryptographic keys. The public key, which can be freely shared, is for encrypting plaintext into ciphertext. The private one is for decrpyting, and must be kept secret.

Node: Any single point on a network. In Veilid, a node is an instance of the veilid-core code run by an application. (for properties of a node, see 2.1. Nodes).

Package manager: software which automatically installs, configures, and removes programs in bulk. Package managers reduce repetition, reduce the odds of a human making a mistake, and ensure each program comes from a trusted source. The package manager that Rust (and therefore Veilid) uses is called “Cargo”.

Repository: The place where your software packages are stored. Repo for short. For Rust projects, this is crates.io

Routing: the process of selecting a path for traffic in a network; which nodes will receive individual network packets on their way to their end destination.

Rust: The programming language that Veilid is built in. To learn more about what makes Rust unique, see their documentation here. (https://www.rust-lang.org/)

Safety route: A route designated by a sending node to protect its physical network connection.

Secret route: A route designated by a receiving node to protect its physical network connection.

Simple schema: A DHT schema type which allows the creator keypair to delegate the ability to write to subkeys to some other keypair or keypairs.

Topology: the structure of a computer network; it’s a description of how data is sent from one device to another. A network topology could be centralized around a central authority, decentralized/federated between many, or distributed across an entire peer-to-peer community.

Tunneling: Moving data from one network to another.

Veilid-core: The main library used to create a Veilid node and operate it as part of an application.

Writer keypair: A keypair owned by someone the creator of a DHT record has given the ability to write to a subkey or several subkeys.
