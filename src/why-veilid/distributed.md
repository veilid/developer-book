# Thinking Distributed

Veilid is a distributed network framework.  When building to take advantage of it's strengths, it is best to Think Distributed.

## What is Distributed Networking

A distributed network is one where data, processing, resources, and even tasks are spread among a number of devices connected to a network like the internet.  This decentralized structure enhances fault tolerance, load balancing, and redundancy, as it eliminates single points of failure. In a distributed network, each node (computer or device) can operate independently while still being part of the larger network, allowing for efficient resource utilization and improved performance.

There are a number of types of distributed networks. 

 * Client-Server Networks: In this model, client devices request services and resources from centralized servers. The server processes these requests and sends back the required data or services. This architecture is widely used in web applications and enterprise environments.

 * Peer-to-Peer (P2P) Networks: Each node in a P2P network can act as both a client and a server, sharing resources directly with other nodes. This decentralized approach is often used for file sharing and blockchain technologies.

 * Mesh Networks: In a mesh network, each node is connected to multiple other nodes, creating a robust and resilient network topology. This allows for dynamic routing of data, ensuring communication can continue even if some nodes fail.

 * Three-Tier Networks: This architecture separates the network into three layers: presentation, application, and data. Each layer is managed by different servers, improving scalability and manageability.

 * N-Tier Networks: Similar to three-tier networks but with more layers, N-tier networks further distribute tasks across multiple servers, enhancing performance and fault tolerance.

Veilid is a peer-to-peer network. P2P networks are decentralized network architectures where each node, or peer, can act as both a client and a server. This means that every participant in the network can request and provide resources, such as files or processing power, directly to and from other peers without the need for a central server. Along with providing network robustness and scalability, it provides for a very effective privacy mechanism and dramatically reduces single points of failure.

One of the key advantages of P2P networks is their ability to efficiently utilize the collective bandwidth and processing power of all connected devices. This makes them particularly suitable for applications like file sharing, distributed computing, and mobile applications. However, P2P networks also face challenges, such as security vulnerabilities and the potential for illegal file sharing. To mitigate these issues, P2P frameworks like Veilid incorporate encryption and other security measures to protect data and ensure the integrity of the network.

## Why Distributed Networking

We've seen what happens when large corporations hold our data. The alternative is to spread the load among those who use the network. Developers have for years shared processing power among several computers networked together.  In the 90s, Matt Curtin worked on a team to prove weakness of DES using a shared computer load via the internet.  Even your humble author worked on a project called DAMPP which used Java Applets to render 3D images over networked computers, sharing the load.

Today things are a bit more complicated than that simple time when sharing an IP of a machine was enough to interact.  With NATted networks, firewalls, mobile, wifi, IoT devices, and more, the network requirement of sharing that load becomes complex - especially when considering the privacy required by the needs ot today's users.

That privacy consideration drives the need for peer to peer distributed networking. Cloud computing and software-as-a-service seemed like such a sensible idea at the time but it turned out that it's just Someone Else's Computer (tm). 

![A distributed network](images/distributednetwork.png)

P2P Distributed networks fo not have a central server.  The diagram above looks like a mess because it is.  Instead of a central server providing command and control, the messaging protocol is designed to keep each endpoint up to date with exactly what they need to know - nothing more, nothing less.

That command and control system in the center of a traditional client/server network has to be owned by someone.  No one owns the center of a P2P distributed network.  That is Why Distributed Networking.

## How To Design Apps For Distributed Networking

Writing apps for a distributed framework requires a certain frame of mind to take advantage of the unique strengths, but avoids the pitfalls of peer-to-peer networks. Some apps benefit greatly from distributed networking, like chat apps for instance.  Some are able to use some of the benefits, like file collaboration.  Some are not well suited to the strategy at all, like apps that collect distributed information for use.

Once you are comfortable with how your app idea fits into the distributed environment, it's a good idea to determine what information is to be shared and establish a message format.  For instance, a chat application should have a invite message, and an accept message, and content.  A little wordsmithing might be in order, but the important thing is to determine what the app will pass on to other nodes on the P2P network.

From there your P2P network framework will kick in.  Communication of the app's needs to the network differs from framework to framework, but in general it will need to have an identifier, and endpoint, and data.  Again, the format will vary, but the general principles hold.

When designing an app for a Veilid P2P network, your application will be insulated from the complexities of encryption and privacy considerations.  The message formation, however, will define the overall quality and maintainability of the app. There is a lot of guidance on how best to do that in the Concepts section.


