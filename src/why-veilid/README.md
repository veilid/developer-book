# Why Veilid

Why Veilid indeed?  Every platform has a network stack, every framework has a security library.  Surely there are existing tools that do what Veilid brings to the table.

The short answer is that Veilid is truly unique. A longer answer takes some time, and is the reason for this section and much of this book.

Veilid exists to finish the job that the IETF got distracted from. It is completely encrypted. It provides means to obscure the IP of any and every node on the network that is communicating with any other node at an application level. It is completely (barring initial bootstrap) peer to peer. When you send requests to the rest of the network, your IP is not visible to the nodes that act on your request. When you want to run services on the Veilid network, you don't have to share your actual IP or even node identity -- there's a mechanism in place to let you obscure your node identity behind a private route, which routes incoming messages to you through several other nodes.

It has a DHT  key-multivalue store, which allows you to migrate most of your configuration away from the device where the app is installed. It will have (but does not currently have) a block store to store arbitrary files. It is mobile-first, which means that the performance characteristics of mobile devices (intermittent network connections, IP addresses which change often, CGNAT, relatively low space, relatively low bandwidth) are fundamentally addressed in the design and not reactively patched. 

It does not need to run on the main Veilid network. For development or compliance reasons, this is an important point. It provides reliable messaging to other nodes, so communications apps can be built to be reliable. It  operates in user space, on non-root ports, so any user account can run it if they need to.

And, the big reason: it's not beholden to any central corporation to operate. The messages you send cannot be harvested or datamined. Your privacy is much more assured than Whatsapp or Signal or Facebook Messenger or any software which phones home to its developer.

It is non-commercialized, and developed entirely by volunteers. It's freely available and open-source as well, so you can independently verify all of these claims.

In this section on why we built Veilid, and why you should use it, you'll find three principal sections.

* [Network topology](networking.md) includes distinctions between three primary topologies, and why Veilid chose the Distributed/P2P strategy.

* [Thinking Distributed](distributed.md) covers how Veilid uses network strategy to keep data in your hands and out of the hands of Big Tech.

* [Long Term Goals](goals.md) will help give you an idea of where we are headed with this project.
