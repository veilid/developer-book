# Nodes

A node is an instance of the veilid-core code run by an application.

Nodes will enable some combination, or all, of the following capabilities:

- ROUT: This node participates in safety and private routing for other nodes.
- SGNL: This node will make reverse connections to nodes which send signal request messages.
- RLAY: This node can relay packets for peers behind restrictive NAT routers.
- DIAL: This node can validate dial info for peers (used by nodes behind NAT routers, to figure out what their world-accessible IP is)
- APPM: This node supports receiving AppMessage and AppCall.
- DHTV: This node participates in the DHT data store.
- DHTW: This node can perform DHT watches on behalf of other nodes for data it caches.
- TUNL: This node can be used for tunneling. (Not currently implemented)
- BLOC: This node participates in the block store. (Not currently implemented)

Nodes have the following properties, as visible to the network:

- A public key, which also serves as its NodeID.
- NodeInfo, which specifies where it can be found on the global Internet, across which protocols, and on what ports. This includes:
  - Its NetworkClass (inbound-capable, outbound-only with signal support, WebApp which requires a relay for outbound connections too)
  - The set of protocols it can connect via (UDP, TCP, WebSocket, and/or WebSocketSecure)
  - What types of addresses it has, IPv4 and/or IPv6
  - The cryptographic suites it can support (Currently, it is a vector which can only contain "VLD0")
  
  - A vector of DialInfoDetail, each of which includes:
    - DialInfoClass, which declares what kind of hoops must be jumped through for it to communicate with the rest of the Veilid network
    - DialInfo, which specifies how to connect to it via UDP, TCP, WebSocket, and WebSocketSecure protocols. THIS CONTAINS IP INFORMATION.

Because a node always has a public key, it also necessarily has a private key associated with that public key. This private key is used to authenticate messages which originate from that node, including the announcements of its own NodeInfo (as SignedNodeInfo).

A node which offers SGNL and RLAY must not require SGNL or RLAY communicate with the network (specifically, not to be 'relayed' DialInfo, and not to be 'OutboundOnly' network class).
