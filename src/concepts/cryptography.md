# Cryptography

Encryption is the process of obscuring a piece of information, so that it can only be understood by the intended recipient. Like spelling out V-E-T because your dog knows the word veterinarian.

Human beings are marginally more intelligent than dogs, so we need to work a little harder in order to encrypt things from each other. We need math; an algorithm that can turn a plaintext input into an encrypted output too difficult for humans to guess, and then turn it back again later.


# Public & Private Keys

A key is a string of numbers and letters which, when run through that algorithm, can be used to encrypt or decrypt a piece of data.

In asymmetric, or “public key”, encryption, a different key is used for each encrypting and decrypting. Together, they form a “keypair”.

The key used for encrypting is public; it can be shared freely, and anyone can use it to encrypt data. The key used for decrypting is private; it must be known only to the intended recipient. If the private key is ever leaked, a new set must be issued in order to send data securely.
