# NAT Traversal

Network Address Translation (NAT) Traversal is a collection of techniques that are designed to maintain TCP/IP connections across routers that implement NAT. These techniques are vital in peer to peer networks because few devices have a public IP in this current era of the internet.  As such, use of NAT traversal techniques are core to Veilid.

## Traversal Techniques

With the explosion of mobile devices, NAT traversal concepts are many and varied. 

STUN (Session Traversal Utilities for NAT) helps devices discover their public IP address and the type of NAT they are behind. By querying a STUN server, a device can learn its public-facing IP address and port, which is crucial for establishing direct peer-to-peer connections. 

TURN (Traversal Using Relays around NAT) assists in scenarios where direct peer-to-peer communication is not possible due to restrictive NAT types or firewalls. It relays data through a server when direct communication is blocked, ensuring the connection remains stable. 

ICE (Interactive Connectivity Establishment) combines STUN and TURN to find the best path for communication. It tries multiple methods to establish a connection, including direct and relay paths, and selects the most efficient one. UDP Hole Punching establishes a direct connection between two devices behind NATs. Both devices send UDP packets to a third-party server, which then helps them establish a direct connection by "punching" through the NAT. 

UPnP (Universal Plug and Play) allows devices to automatically configure network settings to enable NAT traversal. Devices use UPnP to request the router to open specific ports, facilitating direct communication.

## Veilid NAT Traversal (VICE)

Veilid handles the NAT traversal for apps running on the network.  Veilid implements its NAT traversal in a fashion modeled after STUN, but completely in-network without the use of specialized 'STUN servers'. Veilid relaying is like TURN, but is only applied when no other STUN-like establishment can be applied. Veilid's connectivity establishment mechanism is inspired by these but is not an implementation of any of them. We call it Veilid Internet Connectivity Establishment, or VICE.

VICE performs a STUN-like operation called 'public address detection', then uses inbound relays chosen by each node to perform 'signalling'. Signalling is like the first step of ICE, and negotiates reverse connection and hole punching. If those fail, VICE falls back to inbound relaying, using the same servers it did for signalling to to effect a TURN-like relay-based connection.

This is finally all wrapped up into two phases: 'validation of dial info', done far ahead of time, and a 'contact method' calculation, done at connection establishment time. When the message needs to go out, Veilid just runs the contact methods in order until one works. 