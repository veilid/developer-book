# Introduction

Veilid is the networking platform and protocol created, implemented, and maintained by the Veilid Foundation.

We exist to develop, distribute, and maintain a privacy focused communication platform and protocol for the purposes of defending human and civil rights.

> "Fight for the things you care about, but do it in a way that will lead others to join you." -Ruth Bader Ginsburg

This developer book gives application owners insight into the philosophy, design, and implementation details of Veilid.  It is paired with a forthcoming internals book. This book is broken into four principal sections:

* [Why Veilid](why-veilid/index.html), an overview of why using Veilid will bring privacy and security into your peer to peer application.
* [Concepts](concepts/index.html) provides key details about the core ideas that make Veilid work.
* The [Admin](admin/index.html) section has an overview of getting Veilid running to best suit your environment, and keeping it running.
* Finally, [Apps](apps/index.html) shows samples of using the Veilid API in various platforms like Python and Dart.