# Usage

First, run `veilid-server`. This demo tries to connect to localhost port 5959 by default. You can override that with the `--host` and `--port` arguments.

Create your cryptographic keypair:

```console
$ poetry run chat keygen
Your new public key is: d3aDb3ef
Share it with your friends!
```

This writes your new private key to the local keystore. Do _not_ store your local keys to other files, they are not encrypted!

Copy the public key and send it to a friend you want to chat with. Have your friend do the same.

Now, add your friend's public key to your keyring:

```console
$ poetry run chat add-friend MyFriend L0nGkEyStR1ng
```

To start a chat with that friend:

```console
$ poetry run chat start MyFriend
New chat key: VLD0:abcd1234
Give that to your friend!
SEND>
```

Copy that chat key and send it to your chat partner. They can respond to your chat:

```console
$ poetry run chat respond CoolBuddy VLD0:abcd1234
SEND>
```

Now you can send simple text messages back and forth. Your messages are encrypted and transmitted through a distributed hash table (DHT), passed through an open-source, peer-to-peer, mobile-first networked application framework. Neat!

Remember that this simplified program can only receive a message when it's not waiting for you to enter one.
