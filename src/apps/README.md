# Veilid Applications

Every Veilid application is a node on the Veilid network.  The app's traffic, and traffic from other apps, will travel through that app's built in node in a secure, private way.

# The Veilid API

Understanding of the Veilid API is the first item on the task list. At some point, even at the highest level, and app developer will have to use the Veilid API. The internals of the application are accessible from platform-specific examples, and njson endpoints, in the main Veilid repo:

* [Flutter](flutter-example) (veilid-flutter)
* [Python](python-chat) (veilid-python)
* [WASM](wasm-example) (veilid-wasm)

[VeilidChat](veilidchat) is an example of a finished project, node to UI, ready to deploy to an app store near you.  It is at a [seperate repo](https://gitlab.com/veilid/veilidchat), and has app-store ready builds that already live in the App Store and the Play Store.  The code is a great example of using the API, and handling unit testing, in the entire flutter platform.

# Buillding a Veilid App

If you want to get started right away, we certainly don't blame you - we are too! The easiest way to get started is in Flutter.  In the Veilid project the veilid-flutter project referenced above and in the later section of the book will get you started the fastest.

If you would rather work in Python, that's cool too.  There is a fantastic [sample app for Python](https://gitlab.com/veilid/python-demo) also in the Veilid repo that has excellent inline comments to help make conversation with the Veilid network possible. 

The Python API does require reference to a local Veilid Server install. Don't let the name fool you, though. If you are constrained, it can be configured to provide no special services to the network, and turn it into a server that only really serves Veilid network and secure data storage access to your Python app.

To get as close to the metal as it gets, you will be working in Rust. The good news is that the crates are published, ready, and work well! You can find them on the [Rust site](https://docs.rs/veilid-core/.

The long and short of it is, to get a quick app put together (Event planner anyone?) work in Flutter.  For a lot of flexibility, give Python a try. To build structural pieces, give Rust a look. And stay involved!  Let everyone know what you are working on in the [official Discord](https://discord.com/invite/5Qx3B9eedU), [unofficial subreddit](https://www.reddit.com/r/veilid_framework/), or [Mastodon](https://hackers.town/@VeilidNetwork)! 