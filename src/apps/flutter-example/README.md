# Veilid Flutter Example

To set up a new Veilid enabled Flutter project, you can start by grabbing the veilid-flutter directory out of the main project, and putting it right alongside the Veilid directory in your projects folder. You can also start with the VeilidChat application since it is made with Flutter as well.

## Flutter projects at 10,000 Feet

Get in the blimp, we are looking at this from way above.
1. Clone veilid
2. Clone veilid-chat
3. Run veilid/dev-setup/install_linux_prerequisites.sh
4. Run veilid/dev-setup/setup_linux.sh
5. Run veilid-chat/dev-setup.sh
6. Run veilid-chat/build.sh
7. Install your development environment and install its flutter plugin
8. Open veilid-chat project in your IDE
9. Adjust the configuration between Veilid, Flutter, and the operating system as needed
10. Build and deploy

## Step by step writeups for different environments

* [Using Android Studio in Debian](androidstudio.md)
* [Working in Linux on the command line](linux.md)
* [Building Windows apps](windows.md) 