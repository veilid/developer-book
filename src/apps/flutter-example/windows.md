# Building Veilid Flutter apps on Windows

For Windows, starting with the VeilidChat application to structure your own application is a really fine idea.  There are tricky build considerations that have already been worked through, and reusing that code is a good thing.

The first thing you need is the Veilid and VeilidChat source code, from Gitlab.  Open an administrative command prompt, navigate to your default coding folder (I use `c:\\code`, YMMV), and clone like crazy:

git clone https://gitlab.com/veilid/veilid.git

git clone https://gitlab.com/veilid/veilidchat.git

## Getting set up

There are more than a few bits you might need to add to your machine.  If you are starting relatively from scratch, you'll need all of this.  If you already have Rust or Flutter, then you can obviously skip those.

CapnProto is a binary encoding library, and provides some features needed to translate between Rust and C++ and Dart. I know it seems like a joke application on the site, but it is serious software.

https://capnproto.org/install.html

Speaking of Rust, Veilid is written in Rust, we might need that.

https://www.rust-lang.org/tools/install

# Building VeilidChat

Not it's time to run some scripts. In the administrative command prompt launched above, navigate to `/veilid` and run:

`dev-tools/setup.bat`

Assuming no errors, switch over to `/veilidchat` and get started there.  First, though, you will need Flutter.

One of the things that need to happen here is installation of Visual Studio 2022 Community Edition from the link on Flutter's install instructions. This is different from VScode, a common confusion.  Microsoft is not so good at naming things.  Make sure you install with the "Desktop Development with C++ Workload."

https://docs.flutter.dev/get-started/install/windows/desktop

Flutter isn't as much of an installer as it is an "unpack and edit the path" kind of thing.  I followed the instructions on the Flutter page, and you should too because they are subject to change.

Note that you will need CMD not PS terminal to run flutter.  From a prompt, run:

`flutter install windows --release`

And select 1 for Windows.

![Select 1 for Windows install](images/flutterinstallwindows.png)

This will clean set up for the full build., which is:

`reset_run.bat`

It will ask once again if you want to work with Windows or another installed platform. for this, again, select Windows. Then, we wait.

![VeilidChat in all its Windows glory.](images/veilidrunningwindows.png)

# Using VSCode

Coming soon! We wanted to get the above out sooner rather than later.