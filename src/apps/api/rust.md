# Rust API

The [rustdoc for the veilid-core crate](https://docs.rs/crate/veilid-core/) will *always* be more authoritative than this book. If you have specific improvements for the rustdoc, please open a ticket at [the Veilid issue tracker](https://gitlab.com/veilid/veilid/issues/).
