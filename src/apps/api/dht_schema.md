# DHT Schema

In Veilid, the DHT (Distributed Hash Table) is a set of "records" which are stored across nodes on the network. A record has a "lookup key" and a "value". The lookup key is used to look up (and potentially retrieve from other nodes) the value of the record. The value is composed of a "schema" and a list of "subkey values", where each subkey has an ordinal index (0, 1, 2, etc).

The schema is a declaration of who can store information under a given sub-key, and is used to validate attempts to update or write to those subkey values. Depending on the schema form used, the creator of the DHT record (the 'owner') or one of a selected set of 'writers' listed in the record's schema will be allowed to write to a given subkey.

There are (as of 0.2.6) two types of schema. One, tagged "default" or "DFLT", allows only the keypair of the creator of the DHT entry to store information in it or its subkeys.

The other type of schema was created for a simple form of a social media, and is tagged "simple" or "SMPL". It allows the schema creator to assign (at creation time) permission to write to other subkeys to other signing keypairs.

Once created, the schema cannot be changed, because it is hashed to form part of the lookup key. Any alterations would change the hash, and thus also change the name of the DHT record.
