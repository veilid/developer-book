# Configuring Veilid Applications

Configuring applications for Veilid depends on the platform, but some core parts are the same. Every Veilid application is also a Veilid server, and veilid servers have configuration.

Veilid-server.conf is the core of the Veilid server that is at the core of your Veilid application.  That file defines the logging, network ports, and directories for storage. That application, under most circumstances, will not need to interact with those details at all.  It is insulated from those details by the API.

## Logging

Irrespective of the application, logging is core to keeping an eye on the internals of the network communication.  Developers using veilid-core do not use veilid server or its config file directly, but they set the config through the VeilidConfig object during api startup. Logging is done separately. Each language has different logging bindings.

### Rust

For rust it is done through a Tracing layer. At the rust level instantiate a veilid_core::ApiTracingLayer add it to the Layer stack. There's log 'facilities' you can enable and disable, and a default list of facilities that are ignored. These are the facilities for which logs are not emitted by default and you'd have to 'ignore' those log targets (log target is the same as 'facility', terminology-wise).

```
pub static DEFAULT_LOG_FACILITIES_IGNORE_LIST: [&str; 29] = [
    "mio",
    "h2",
    "hyper",
    "tower",
    "tonic",
    "tokio",
    "runtime",
    "tokio_util",
    "want",
    "serial_test",
    "async_std",
    "async_io",
    "polling",
    "rustls",
    "async_tungstenite",
    "tungstenite",
    "netlink_proto",
    "netlink_sys",
    "hickory_resolver",
    "hickory_proto",
    "attohttpc",
    "ws_stream_wasm",
    "keyvaluedb_web",
    "veilid_api",
    "network_result",
    "dht",
    "fanout",
    "receipt",
    "rpc_message",
];
```

These are the *enabled* facilities: 

```
pub static DEFAULT_LOG_FACILITIES_ENABLED_LIST: [&str; 8] = [
    "net",
    "rpc",
    "rtab",
    "stor",
    "client_api",
    "pstore",
    "tstore",
    "crypto",
];
```
### Dart

For Dart programs, with FFI, you instantiate a VeilidFFIConfig object with your logging config.  Here is an example from `veilid-flutter/example/lib/veilid_init.dart` in the example code for veilid-flutter: 

```
// Initialize Veilid
// Call only once.
void veilidInit() {
  if (kIsWeb) {
    const platformConfig = VeilidWASMConfig(
        logging: VeilidWASMConfigLogging(
            performance: VeilidWASMConfigLoggingPerformance(
                enabled: true,
                level: VeilidConfigLogLevel.debug,
                logsInTimings: true,
                logsInConsole: true),
            api: VeilidWASMConfigLoggingApi(
                enabled: true, level: VeilidConfigLogLevel.info)));
    Veilid.instance.initializeVeilidCore(platformConfig.toJson());
  } else {
    const platformConfig = VeilidFFIConfig(
        logging: VeilidFFIConfigLogging(
            terminal: VeilidFFIConfigLoggingTerminal(
              enabled: false,
              level: VeilidConfigLogLevel.debug,
            ),
            otlp: VeilidFFIConfigLoggingOtlp(
                enabled: false,
                level: VeilidConfigLogLevel.trace,
                grpcEndpoint: 'localhost:4317',
                serviceName: 'VeilidExample'),
            api: VeilidFFIConfigLoggingApi(
                enabled: true, level: VeilidConfigLogLevel.info),
            flame: VeilidFFIConfigLoggingFlame(enabled: false, path: '')));
    Veilid.instance.initializeVeilidCore(platformConfig.toJson());
  }
}
```

### Python 

Python doesn't have FFI or WASM support yet, and uses the JSON API only right now so that required veilid-server, and just uses the veilid-server.conf.  That's easy because you can just edit the file!

```
logging:
  system:
    enabled: true
    level: info
  terminal:
    enabled: false
```

## Core



